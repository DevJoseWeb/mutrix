function love.conf(t)
    t.title = "Mütrix"
    t.author = "Simon Larsen"
	t.identity = "mutrix"
    t.screen.width = 37*16
    t.screen.height = 28*16
    t.screen.fullscreen = false
    t.modules.joystick = false
    t.modules.physics = false
end
